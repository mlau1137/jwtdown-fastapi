### v0.1.6

* Getting the build artifacts to PyPi, again.

### v0.1.5

* Handle build artifacts smarter.
* Handle build caches smarter.

### v0.1.3

* Build and deploy Read The Docs site.

### v0.1.2

* Publish build artifacts to PyPi.

### v0.1.1

* Build artifacts only on tags
* Test all pushes

### v0.1.0

* Initial import.
